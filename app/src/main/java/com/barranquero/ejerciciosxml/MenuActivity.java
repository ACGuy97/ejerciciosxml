package com.barranquero.ejerciciosxml;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }

    public void onClickMenu(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.btnPartesXML:
                intent = new Intent(this, Partes.class);
                startActivity(intent);
                break;
            case R.id.btnNotas:
                intent = new Intent(this, Notas.class);
                startActivity(intent);
                break;
            case R.id.btnRSS:
                intent = new Intent(this, TitularesRss.class);
                startActivity(intent);
                break;
            case R.id.btnNoticias:
                intent = new Intent(this, Noticias.class);
                startActivity(intent);
                break;
            case R.id.btnCrear:
                intent = new Intent(this, Creacion.class);
                startActivity(intent);
                break;
        }
    }
}
