package com.barranquero.ejerciciosxml;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.barranquero.ejerciciosxml.modelo.Noticia;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by usuario on 21/11/16.
 */

public class Noticias extends AppCompatActivity {
    public static final String CANAL = "http://www.europapress.es/rss/rss.aspx?ch=279";
    //public static final String CANAL = "http://10.0.2.2/feed/europapress.xml";
    public static final String TEMPORAL = "europapress.xml";
    FloatingActionButton fabObtenNoticias;
    ListView lista;
    ArrayList<Noticia> noticias;
    ArrayAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_noticias);
        setTitle("Noticias");

        lista = (ListView)findViewById(R.id.lvToDoList);
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Uri uri = Uri.parse(noticias.get(i).getLink());
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                if (intent.resolveActivity(getPackageManager()) != null)
                    startActivity(intent);
                else
                    Toast.makeText(getApplicationContext(), "No hay un navegador", Toast.LENGTH_SHORT).show();
            }
        });

        fabObtenNoticias = (FloatingActionButton)findViewById(R.id.fabObtenNoticia);
        fabObtenNoticias.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                descarga(CANAL, TEMPORAL);
            }
        });
    }

    private void descarga(String canal, String temporal) {
        final ProgressDialog progreso = new ProgressDialog(this);
        File miFichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), temporal);
        RestClient.get(canal, new FileAsyncHttpResponseHandler(miFichero) {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Conectando . . .");
                progreso.setCancelable(false);
                progreso.show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                progreso.dismiss();
                Toast.makeText(Noticias.this, "\"Fallo: \" + statusCode + \"\\n\" + throwable.getMessage()", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                progreso.dismiss();
                try {
                    //informacion.setText(Analisis.analizarRSS(file));
                    noticias = Analisis.analizarNoticias(file);
                    mostrar();
                    Toast.makeText(Noticias.this, "Hecho", Toast.LENGTH_SHORT).show();
                } catch (XmlPullParserException e) {
                    Log.e("XMP ", e.getMessage());
                } catch (IOException e) {
                    Log.e("IO ", e.getMessage());
                }
            }
        });
    }

    private void mostrar() {
        if (noticias != null) {
            if (adapter == null) {
                adapter = new ArrayAdapter<Noticia>(this, android.R.layout.simple_list_item_1, noticias);
                lista.setAdapter(adapter);
            } else {
                adapter.clear();
                adapter.add(noticias);
            }
        } else
            Toast.makeText(getApplicationContext(), "Error al crear la lista", Toast.LENGTH_SHORT).show();
    }
}

