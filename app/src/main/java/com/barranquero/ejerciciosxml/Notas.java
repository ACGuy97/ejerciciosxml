package com.barranquero.ejerciciosxml;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * Created by usuario on 21/11/16.
 */

public class Notas extends AppCompatActivity {
    TextView txvNotas;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notas);
        setTitle("Notas alumnos");

        txvNotas = (TextView)findViewById(R.id.txvNotas);
        try {
            txvNotas.setText(Analisis.analizarNombres(this));
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
