package com.barranquero.ejerciciosxml;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.barranquero.ejerciciosxml.modelo.Noticia;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Created by usuario on 21/11/16.
 */

public class Creacion extends AppCompatActivity {
    public static final String RSS = "http://www.alejandrosuarez.es/feed/";
    public static final String TEMPORAL = "alejandro.xml";
    public static final String FICHERO_XML = "resultado.xml";
    Button boton;
    ArrayList<Noticia> noticias;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creacion);
        setTitle("Crear XML");

        boton = (Button)findViewById(R.id.btnCrear);
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                descarga( RSS , TEMPORAL );
            }
        });
    }

    private void descarga(String rss, String temporal) {
        final ProgressDialog progreso = new ProgressDialog(this);
        File miFichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), temporal);
        RestClient.get(rss, new FileAsyncHttpResponseHandler(miFichero) {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Conectando . . .");
                progreso.setCancelable(false);
                progreso.show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                progreso.dismiss();
                //informacion.setText("Fallo: " + statusCode + "\n" + throwable.getMessage());
                Toast.makeText(Creacion.this, "Fallo: "+statusCode, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                progreso.dismiss();
                try {
                    //informacion.setText(Analisis.analizarRSS(file));
                    noticias = Analisis.analizarNoticias(file);
                    Analisis.crearXML(noticias, FICHERO_XML);
                } catch (XmlPullParserException e) {
                    Log.e("XMP ", e.getMessage());
                } catch (IOException e) {
                    Log.e("IO ", e.getMessage());
                }
            }
        });
    }
}
