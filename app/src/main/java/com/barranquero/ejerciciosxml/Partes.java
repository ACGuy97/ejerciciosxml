package com.barranquero.ejerciciosxml;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * Created by usuario on 21/11/16.
 */

public class Partes extends AppCompatActivity {
    TextView txvInfo;
    public static final String TEXTO = "<texto><uno>Hello World!</uno><dos>Goodbye</dos></texto>";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_partes);
        setTitle("Partes XML");

        txvInfo = (TextView)findViewById(R.id.txvInfo);
        try {
            txvInfo.setText(Analisis.analizar(TEXTO));
        } catch (XmlPullParserException e) {
            Log.e("PullParserEx", e.getLocalizedMessage());
        } catch (IOException e) {
            Log.e("IOEx", e.getLocalizedMessage());
        }
    }
}
